#!/usr/bin/python3


def even_list(lst):
    """Accepts a list of integers and returns a new list
    containing even numbers from even indices of the original list.
    """
    if not all(isinstance(x, int) for x in lst):
        raise TypeError("lst must only contain integers")
    evens = []
    for idx, val in enumerate(lst):
        if idx % 2 == 0 and val % 2 == 0:
            evens.append(val)
    return evens


def nested_even_list(lst, n):
    """Accepts a nested list of variable depth, and the length of
    that list. Unpacks the list, then returns the even numbers from
    even indices. Lst must only contain integers, or lists of integers."""
    evens = []
    for item in lst:
        if isinstance(item, list):
            evens.extend([x for x in item])
        if isinstance(item, int):
            evens.append(item)
    local_length = len(evens)
    if local_length == n:
        # list is unpacked, return even values
        return even_list(evens)
    else:
        return nested_even_list(evens, local_length)
