#!/usr/bin/python3
import unittest
from even_list import even_list, nested_even_list


class EvenListTest(unittest.TestCase):
    def setUp(self):
        self.floats = [2.0, 4.0, 3.0]
        self.multi_type = [2, 1.0, "3"]
        self.odd_vals = [1, 1, 1]
        self.odd_idx = [1, 2, 3, 4]
        self.even_vals_even_idx = [2, 1, 4, 3]

    def test_non_integers(self):
        with self.assertRaises(TypeError):
            even_list(self.floats)

    def test_list_of_various_types(self):
        with self.assertRaises(TypeError):
            even_list(self.multi_type)

    def test_odd_values(self):
        output = even_list(self.odd_vals)
        self.assertEqual(0, len(output))

    def test_odd_indices(self):
        output = even_list(self.odd_idx)
        self.assertEqual(0, len(output))

    def test_even_vals_even_idx(self):
        output = even_list(self.even_vals_even_idx)
        self.assertEqual([2, 4], output)


class NestedEvenListTest(unittest.TestCase):
    def setUp(self):
        self.multi_type = [4, "1", [2.0, 3]]
        self.nested = [2, [2, 4, [4, 5, 6, 8, 8]]]
        self.layers = [1, [1, 2], [4, 4], [5, [6, 8]], 6, [2, 1]]

    def test_nested_various_types(self):
        with self.assertRaises(TypeError):
            nested_even_list(self.multi_type, len(self.multi_type))

    def test_nested(self):
        output = nested_even_list(self.nested, len(self.nested))
        self.assertEqual([2, 4, 8], output)

    def test_nested_layers(self):
        output = nested_even_list(self.layers, len(self.layers))
        self.assertEqual([2, 4, 6, 6], output)
